import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class PaymentTracker {

    //Collection to save all the payment records in CURRENCY VALUE format
    private ArrayList<Pair> paymentRecords;

    //Collection to save an actual account balance of each currency
    private HashMap<String, Float> moneyBalance;


    PaymentTracker(ArrayList<Pair> paymentRecords, HashMap<String, Float> moneyBalance){
        this.paymentRecords = paymentRecords;
        this.moneyBalance = moneyBalance;
    }

    //Method to load input from a file
    void loadFromFile(String file){

        //Path to the file from working directory
//        String fileName = "src\\main\\java\\" + file;
        String fileName = file;

        //String for saving line of the file
        String line = null;

        try{

            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            //Line number used in an error message
            int lineNumber = 0;

            System.out.println("Reading from file...");

            while ((line = bufferedReader.readLine()) != null){
                lineNumber++;
                //Input validation
                if(validateInput(line)){
                    //Adding record to memory
                    addRecord(line, paymentRecords, moneyBalance);
                }else{
                    //Error in file message output
                    System.out.println("Error in line " + lineNumber + " of file \"" + fileName + "\". Record won't be inserted.");
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Unable to open file \"" + fileName + "\"");
        } catch (IOException e) {
            System.out.println("Error reading file \"" + fileName + "\"");
        }

    }

    //Method repeatedly asking for user input until "quit" keyword is not used
    void askForUserInput(){

        String userInput;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Enter payment: ");
            userInput = scanner.nextLine();

            if((!userInput.equals("quit")) && validateInput(userInput)){
                //Adds valid input into the program memory
                addRecord(userInput, paymentRecords, moneyBalance);
            }
        } while (!userInput.equals("quit"));

        scanner.close();
    }

    //Adds records to program memory
    private void addRecord(String record, ArrayList<Pair> paymentRecords, HashMap<String, Float> moneyBalance){

        //Splits record to CURRENCY and VALUE pair by " " (space) character
        String[] splittedRecord = record.split(" ");

        String currency = splittedRecord[0].toString();
        float value = Float.parseFloat(splittedRecord[1]);

        //Saves record into the memory as a Pair into the ArrayList
        paymentRecords.add(new Pair(currency, value));

        /*Adds record to account balance:
         *  - If collection does not contain currency, it adds it also with a value
         *  - If collection contains currency already, it just adds (sum) a value to actual value
         */
        moneyBalance.merge(currency, value, Float::sum);
    }

    //Method for input validation
    private Boolean validateInput(String userInput){

        //Check regexp (3 uppercase letters + space + optional minus sign + numbers with/without floating point)
        if(!userInput.matches("[A-Z][A-Z][A-Z]\\s[-]?(\\d+)([.]+(\\d+))*")){
            System.out.println("Wrong format!");
            System.out.println("Example of correct format: \"USD 100\", \"EUR -40\", \"MYR -3.65\", ...");
            return false;
        }

        //Checks if currency exists
        for (Currency currency : getAllCurrencies()) {
            if (userInput.substring(0, 3).equals(currency.toString())){
                return true;
            }
        }

        System.out.println("Currency does not exist!");
        return false;
    }

    //Method to get a Set of all existing currencies in the world
    private Set<Currency> getAllCurrencies(){

        Set<Currency> currencies = new HashSet<Currency>();
        Locale[] locales = Locale.getAvailableLocales();

        for(Locale loc : locales){
            try {
                Currency currency = Currency.getInstance(loc);
                if (currency != null){
                    currencies.add(currency);
                }
            } catch (Exception e){
                //Locale not found
            }
        }
        return currencies;
    }



}
