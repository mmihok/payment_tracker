import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OutputBalance implements Runnable {

    //Collection to save an actual account balance of each currency
    private HashMap<String, Float> moneyBalance;

    //Collection to save an actual conversion rate from USD to each world currency
    private HashMap<String, Float> conversionRate;


    OutputBalance(HashMap<String, Float> moneyBalance){
        this.moneyBalance = moneyBalance;
    }


    @Override
    public void run() {

        //Downloads actual conversion rate
        this.conversionRate = getConversionRate();

        while(!Thread.currentThread().isInterrupted()) {

            //Outputs actual money balance
            printBalance();

            /*Thread output interrupts output of PaymentTracker class
              It is appropriate to print a message asking for user input again. */
            System.out.println("Enter payment:");

            try {
                //Sleep for one minute
                TimeUnit.SECONDS.sleep(60);
            } catch (InterruptedException e) {
                /*When thread is interrupted during SLEEP, it will throw InterruptedException
                In this case we need to interrupt it again from inside
                */
                System.out.println("Shutting down...");
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    //Outputs actual momeny balance
    private void printBalance(){

        System.out.println("ACCOUNT BALANCE:");

        for (String currency : moneyBalance.keySet())
        {
            //Checks if money balance for specific currency is not equal zero
            if(moneyBalance.get(currency) != 0) {

                //If currency equals "USD" it is not necessary to output its USD conversion rate
                if( currency.equals("USD") ){
                    System.out.println(currency + " " + new DecimalFormat("#.##").format(moneyBalance.get(currency)));

                //Checks if conversionRate collection contains conversion rate for specific currency
                }else if( conversionRate != null && conversionRate.containsKey(currency) ){

                    //Outputs money balance for specific currency together with it's USD conversion rate
                    System.out.println(currency + " " + new DecimalFormat("#.##").format(moneyBalance.get(currency))
                            + " (USD: " + String.format(java.util.Locale.US,"%.2f",
                            (moneyBalance.get(currency)/ conversionRate.get(currency))) + ")");
                }

                //If conversion rate for specific currency is not available, it will output money balance for specific currency with N/A USD conversion rate
                else {
                    System.out.println(currency + " " + new DecimalFormat("#.##").format(moneyBalance.get(currency)) + " (USD N/A)");
                }
            }
        }
    }

    //Loads actual USD conversion rate using REST API
    private HashMap<String, Float> getConversionRate(){

        String url_str = "https://api.exchangerate-api.com/v4/latest/USD";

        //Making request
        URL url = null;
        try {
            url = new URL(url_str);
            HttpURLConnection request = null;
            request = (HttpURLConnection) url.openConnection();
            request.connect();

            //Converting to JSON
            JsonParser jp = new JsonParser();
            JsonElement je = null;

            je = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject jsonobj = je.getAsJsonObject();

            //Gets conversion rates from JSONObject
            JsonObject rates = jsonobj.get("rates").getAsJsonObject();

            //HashMap to save conversion rates
            HashMap<String, Float> conversionRate = new HashMap<>();

            //Iterate through JSONObject's conversion rates and saves it to HashMap
            for (Map.Entry<String, JsonElement> stringJsonElementEntry : rates.entrySet()) {
                conversionRate.put(stringJsonElementEntry.getKey(), stringJsonElementEntry.getValue().getAsFloat());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Could not connect to get conversion rates.");
            System.out.println("Conversion rates will be unavailable.");
        }

        return conversionRate;

    }

}
