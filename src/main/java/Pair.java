//A class to store values in pairs
public class Pair<Element1, Element2> {

    private final Element1 element1;
    private final Element2 element2;

    Pair(Element1 element1, Element2 element2){
        this.element1 = element1;
        this.element2 = element2;
    }

    public Element1 getElement1(){
        return element1;
    }

    public Element2 getElement2(){
        return element2;
    }


    @Override
    public String toString() {
        return element1 + " " + element2;
    }
}
