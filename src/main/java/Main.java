import java.util.*;

public class Main {

    public static void main(String[] args) {

        //Checks if there's not more than one command line parameter
        if(args.length > 1){
            System.out.println("Invalid number of parameters. Maximum is 1.");
            System.out.println("Closing application...");
            return;
        }

        //Collection to save all the payment records in CURRENCY VALUE format
        ArrayList<Pair> paymentRecords = new ArrayList<>();

        //Collection to save an actual account balance of each currency
        HashMap<String, Float> moneyBalance = new HashMap<>();

        //An instance of PaymentTracker class containing main logic of Payment_tracker application
        PaymentTracker paymentTracker = new PaymentTracker(paymentRecords, moneyBalance);

        //Calls method loading input from file passed as an argument to application
        if(args.length == 1) {
            paymentTracker.loadFromFile(args[0]);
        }

        //Declares and initializes a new thread responsible for periodic output of account balance
        Thread outBalThread = new Thread(new OutputBalance(moneyBalance));

        //Starts the thread
        outBalThread.start();

        //Calls method repeatedly asking input from user
        paymentTracker.askForUserInput();

        //Safely interrupts the thread
        outBalThread.interrupt();

    }



}
