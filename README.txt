HOW TO RUN:

	Enter "java -jar Payment_tracker.jar [filename]*" into command line. Input file name is optional.

--------------------------------------------------------------------------------------------------


ASSUMPTIONS:


	PARAMETERS CHECK

		- Application checks if there is 0 or 1 command line parameters. If there's more, it will close.
	

	INVALID INPUT CHECK

		- When user inserts invalid input, there are 2 assumptions:

			- Error message: "Wrong format! Example of correct format: "USD 100", "EUR -40", "MYR -3.65", ..." 
			  All input should be in [currency_code][space][+/-][value] format, any other input is considered as wrong.

			- Error message: "Currency does not exist!" 
			  Application checks if the currency code exists in the world currencies list

			-  These two assumptions also works with file input. 
			   In this case there's also additional message informing about faulty lines,
			   which will be not inserted: "Error in line [line_number] of file [filename]. Record won't be inserted."


	FILE CHECK

		- When working with input file, there are also "Unable to open file [filename]." and "Error reading file [filename]." 
		  messages printed when FileNotFound or IOException are thrown.


	USD CONVERSION RATE AVAILBABILITY CHECK

		- When currency to USD conversion rate is unavailable or there's problem with connection to get conversion rates 
		  (e.g. internet connection is unavailable), the error message 
		  "Could not connect to get conversion rates. Conversion rates will be unavailable." will be shown.



--------------------------------------------------------------------------------------------------

POSSIBILITIES FOR FUTURE IMPROVEMENTS:


	- in case of connection failure try connecting again to get conversion rate
	- check if internet connection is available before connecting
	- make zero value inputs invalid
	- make unit tests


--------------------------------------------------------------------------------------------------
Created by Ing. Marek Mihok
